# Shopify Liquid Snippets

Snippets of code written in liquid for small functionalities on Shopify stores.

## Snippets

1. **Product Quantity**: displays the number of stock remaining. Shows warning if only one or none is available. ([product-quantity.liquid](/scripts/product-quantity.liquid))
1. **Tags in Collection**: displays filter quick buttons for each tag on items in the current collection. Best used if each item has one tag. ([collection-tags.liquid](/scripts/collection-tags.liquid))
1. **Product Cycle**: adds clickable images of the previous and next products in a collection, which takes the customer to the respective products. Only works if the handles are sequential numbers. Also takes tags into consideration. ([product-cycle.liquid](/scripts/product-cycle.liquid))